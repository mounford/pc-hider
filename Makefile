all: liblex.so

liblex.so: hidlex.c
	gcc -Wall -fPIC -shared -o liblex.so hidlex.c -ldl

.PHONY clean:
	rm -f liblex.so
